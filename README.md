# univers

## Description

_univers_ est un écosystème visant à l'amélioration de quelque chose. À vous de choisir à quoi vous l'appliquez.

### Configurer un _univers_ avec des applications

- univers.json
- deployer

### Applications

Choisissez des applications parmi le [catalogue d'applications](apps.md) ou utilisez la configuration par défaut.

### Use-cases

- univers(politiques_publiques)
- univers(univers)

## Écosystème

```mermaid
graph LR
i((inv))
l((log))
e((exe))
s((sim))
w(view/wish)
q((q))
L((loo))

i-->e
l-->e
e-->s
e-->w
w-->q
q-->L
L-->l
```

<!--If mermaid is not rendered:

![Boucle d'applications](https://mermaidjs.github.io/mermaid-live-editor/#/view/eyJjb2RlIjoiZ3JhcGggTFJcbmkoKGludikpXG5sKChsb2cpKVxuZSgoZXhlKSlcbnMoKHNpbSkpXG53KHZpZXcvd2lzaClcbnEoKHEpKVxuTCgobG9vKSlcblxuaS0tPmVcbmwtLT5lXG5lLS0-c1xuZS0tPndcbnctLT5xXG5xLS0-TFxuTC0tPmwiLCJtZXJtYWlkIjp7InRoZW1lIjoiZGVmYXVsdCJ9fQ)
-->

## Changelog

v.0.0.0 : MVP sur papier cf [design](design)

v.0.0.1 : MVP dans les issues gitlab

- inv : [#1](https://gitlab.com/universo/univers/issues/1)
- log : [#2](https://gitlab.com/universo/univers/issues/2)
- exe : [#3](https://gitlab.com/universo/univers/issues/3)
- sim : [#4](https://gitlab.com/universo/univers/issues/4)
- wish: [#5](https://gitlab.com/universo/univers/issues/5)
- q : [#6](https://gitlab.com/universo/univers/issues/6)
- loo : [#7](https://gitlab.com/universo/univers/issues/7)

Deuxième itération

- log : [#8](https://gitlab.com/universo/univers/issues/8)
- exe : [#9](https://gitlab.com/universo/univers/issues/9)
- sim : [#10](https://gitlab.com/universo/univers/issues/10)
- wish: [#11](https://gitlab.com/universo/univers/issues/11)

v.0.0.2 : MVP dans les issues gitlab via l'API de gitlab

L'outil en ligne de commande [univers](univers) uploade les données dans des issues.

Configuration : placer la clé d'API dans ~/secret/gitlab_api_key

Ajouter les droits d'exécution `chmod 777 univers`

Usage : `./univers "<app>" "<data>" "<description>"`

Voir [cmd.sh](cmd.sh) pour des exemples d'exécution de la commande univers
